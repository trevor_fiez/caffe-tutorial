
import os.path









def main():
	class_files = open("data/class_files.txt")
	
	class_list_names = []
	for line in class_files:
		clean_line = line.strip()
		class_list_names.append(clean_line)
		
	
	class_image_names = []
	
	for file_name in class_list_names:
		image_list = open("data/" + file_name, "r")
		class_images = []
		for image_url in image_list:
			image_url = image_url.strip()
			image_name = image_url.split('/')
			image_name = image_name[-1]
			if (os.path.exists("data/" + image_name)):
				if (len(image_name) < 4):
					continue
				print(image_name)
				class_images.append(image_name)
		
		class_image_names.append(class_images)
			
	
	for l in class_image_names:
		print(len(l))
	
	min_length = len(class_image_names[0])
	training_size = int(min_length * 0.75)
	
	training_file = open("training_images.txt", "w");
	test_file = open("test_images.txt", "w");
	labels_file = open("labels.txt", "w");
	for i in range(len(class_image_names)):
		labels_file.write(str(i) + "\n")
		images = class_image_names[i]
		for j in range(len(images)):
			if (j < training_size):
				training_file.write(images[j] + " " + str(i) + "\n")
			else:
				test_file.write(images[j] + " " + str(i) + "\n");
	
	
	
			
	





if __name__ == "__main__":
	main()
