
IMAGENET_BASE="http://www.image-net.org/api/text/imagenet.synset.geturls?wnid="
CLASS_FILE="class_files.txt"
DOG="n02084071"
DOG_NAME="dog_file.txt"
CAT="n02121808"
CAT_NAME="cat_file.txt"

classes=( "bernese.txt" "hound.txt" "newfoundland.txt" "dalmation.txt" "poodle.txt" "husky.txt" "shepherd.txt" "pug.txt" "dachshund.txt" "maltese.txt")
codes=( "n02107683" "n02087551" "n02111277" "n02110341" "n02113335" "n02109961" "n02106662" "n02110958" "n02089232" "n02085936")



mkdir data
cd data

rm -f $CLASS_FILE

for ((i=0;i<${#classes[@]};++i)); do
	wget -O ${classes[i]} $IMAGENET_BASE${codes[i]}
	wget -t 1 -nc --timeout=2 -i ${classes[i]}
	echo ${classes[i]} >> $CLASS_FILE
done

cd ..
