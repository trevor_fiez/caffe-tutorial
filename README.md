# Caffe Tutorial #

This tutorial focuses on the actual steps it takes to run and train caffe.

There is a step-by-step guide, "Training_CNN.pdf", located in the source that takes you through the process of training a dog species classifier from commamnd line with caffe.

It is also located here:

https://docs.google.com/document/d/11GkotQQYZVTAMiTdzOooCqYd14N5Tpr2-DPL-47lgxA/edit?usp=sharing

Obviously Caffe must be installed to run the expiriment.

This tutorial is designed for anyone that wants to use Caffe. It points to other resources that users might find helpful about deep learning and other relevant topics. However, it does not go into those topics.

If you have any idea on how to improve this tutorial please email Trevor Fiez at: fieztr@oregonstate.edu

Good Luck!